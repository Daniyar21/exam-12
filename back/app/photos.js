const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const auth = require("../middleware/auth");
const Photo = require('../models/Photo');


const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});
const upload = multer({storage});
const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const photos = await Photo.find().populate({path: "user", select: "displayName"});
    res.send(photos);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post('/', auth, upload.single('image'), async (req, res) => {
  const imageRoute = 'uploads/';
  if (!req.file) {
    res.status(400).send({error: 'Photo is required'});
  }
  try {
    const photoData = {
      title: req.body.title,
      photo: imageRoute + req.file,
      user: req.user._id,
    };
    const photo = new Photo(photoData);
    await photo.save();
    res.send(photo);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.delete('/:id', auth, async (req, res) => {
  try {
    const photo = await Photo.findByIdAndDelete(req.params.id);
    if (photo.user !== req.user._id) {
      return res.status(401).send({error: 'Permission denied'});
    }
    if (photo) {
      res.send(`Photo '${photo.title} removed'`);
    } else {
      res.status(404).send({error: 'Photo not found'});
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;