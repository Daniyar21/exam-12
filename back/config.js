const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  db: {
    url: 'mongodb://localhost/exam12',
  },
  facebook: {
    appId: "636234800742389",
    appSecret: "0b8349b189238d1314803c3fd453739c",
  },
};