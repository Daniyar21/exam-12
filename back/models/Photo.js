const mongoose = require("mongoose");
const idvalidator = require('mongoose-id-validator');



const PhotoSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  photo: {
    type: String,
    required: true,
  }
});

PhotoSchema.plugin(idvalidator);

const Photo = mongoose.model('Photo', PhotoSchema);

module.exports = Photo;