const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const User = require("./models/User");
const Photo = require("./models/Photo");
const config = require('./config');


const run = async () => {

  await mongoose.connect(config.db.url);
  const collection = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collection) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [aibek, ulan, denis] = await User.create({
    email: 'aibek@gmail.com',
    password: '123',
    token: nanoid(),
    displayName: 'Aibek',
  }, {
    email: 'ulan@gmail.com',
    password: '123',
    token: nanoid(),
    displayName: 'Ulan',
  }, {
    email: 'denis@gmail.com',
    password: '123',
    token: nanoid(),
    displayName: 'Denis',
  });


  await Photo.create({
      user: aibek,
      photo: 'fixtures/1.jpg',
      title: 'Great car!',
    }, {
      user: aibek,
      photo: 'fixtures/2.jpg',
      title: 'Great car!',
    }, {
      user: aibek,
      photo: 'fixtures/3.jpg',
      title: 'Great car!',
    }, {
      user: ulan,
      photo: 'fixtures/4.jpg',
      title: 'Great car!',
    }, {
      user: ulan,
      photo: 'fixtures/5.jpg',
      title: 'Great car!',
    }, {
      user: ulan,
      photo: 'fixtures/6.jpg',
      title: 'Great car!',
    }, {
      user: denis,
      photo: 'fixtures/7.jpg',
      title: 'Great car!',
    }, {
      user: denis,
      photo: 'fixtures/8.jpg',
      title: 'Great car!',
    }, {
      user: denis,
      photo: 'fixtures/9.jpg',
      title: 'Great car!',
    }
  );

  await mongoose.connection.close();
};
run().catch(console.error);