import {Card, CardContent, CardHeader, CardMedia, Grid, makeStyles, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";
import {apiURL} from "../../../config";


const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
})

const PhotoView = (props) => {
    const classes = useStyles();
    const image = apiURL + '/' + props.image;


    return (
        <Grid item xs={12} sm={6} md={3} lg={2}>
            <Card className={classes.card}>
                <CardHeader title={props.title}/>
                <CardMedia
                    image={image}
                    className={classes.media}
                />
                <CardContent>
                    <Typography variant="subtitle1" component={Link} to={'/users/' + props.id}>
                        By: {props.name}
                    </Typography>
                </CardContent>
            </Card>
        </Grid>
    );
};


export default PhotoView;