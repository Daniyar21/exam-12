import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchPhotosRequest} from "../../store/actions/photosActions";
import {CircularProgress, Grid, Typography} from "@material-ui/core";
import PhotoView from "../../components/UI/PhotoView/PhotoView";

const Home = () => {

    const photos = useSelector(state => state.photos.photos);
    const dispatch = useDispatch();
    const loading=useSelector(state => state.photos.fetchPhotosLoading);

    useEffect(()=>{
        dispatch(fetchPhotosRequest());
    },[dispatch]);

    console.log(photos)

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justifyContent="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4">All photos in gallery</Typography>
                </Grid>
            </Grid>
            <Grid item>
                <Grid item container justifyContent="center" direction="row" spacing={1}>
                    {loading ? (
                        <Grid container justifyContent="center" alignItems="center">
                            <Grid item>
                                <CircularProgress/>
                            </Grid>
                        </Grid>
                    ) : photos.map(photo => (
                        <PhotoView
                            key={photo._id}
                            id={photo.user._id}
                            title={photo.title}
                            image={photo.photo}
                            name={photo.user.displayName}
                        />
                    ))}
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Home;