import {all} from 'redux-saga/effects';
import registerUserSaga from "./sagas/usersSagas";
import historySagas from "./sagas/historySagas";
import history from "../history";
import photoSaga from "./sagas/photosSagas";

export function* rootSagas() {
    yield all([
        ...registerUserSaga,
        ...historySagas(history),
        ...photoSaga,
    ])
}