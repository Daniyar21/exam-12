import {createSlice} from '@reduxjs/toolkit';

const name = 'photos';

const photosSlice = createSlice({
    name,
    initialState: {
        photos: [],
        fetchPhotosLoading: false,
        fetchPhotosError: null,
        photosUser:[],
        fetchPhotoUserLoading:false,
    },
    reducers: {
        fetchPhotosRequest: state => {
            state.fetchPhotosLoading = true;
        },
        fetchPhotosSuccess: (state, {payload: photos}) => {
            state.fetchPhotosLoading = false;
            state.photos = photos;
            state.fetchPhotosError = null;
        },
        fetchPhotosFailure: (state, {payload: error}) => {
            state.fetchPhotosLoading = false;
            state.fetchPhotosError = error;
        },
        fetchPhotoUserRequest: state => {
            state.fetchPhotoUserLoading = true;
        },
        fetchPhotoUserSuccess: (state,action) => {
            state.photosUser = action.payload;
            state.fetchPhotoUserLoading = false;
        },
        fetchPhotoUserFailure: state => {
            state.fetchPhotoUserLoading = false;
        },
    }
});

export default photosSlice;