import {put, takeEvery} from "redux-saga/effects";
import {
  loginUser,
  loginUserFailure,
  loginUserSuccess,
  logoutRequest,
  logoutUser,
  registerUser,
  registerUserFailure,
  registerUserSuccess,
    loginFacebook
} from "../actions/usersActions";
import axiosApi from "../../axiosApi";
import {historyPush} from "../actions/historyActions";
import {toast} from "react-toastify";

export function* registerUserSaga({payload: userData}) {
  try {
    const response = yield axiosApi.post('/users', userData);
    yield put(registerUserSuccess(response.data));
    yield put(historyPush('/'));
    toast.success('Registered successful!');
  } catch (error) {
    toast.error(error.response.data.global);
    yield put(registerUserFailure(error.response.data));
  }
}

export function* loginUserSaga({payload: userData}) {
  try {
    const response = yield axiosApi.post('/users/sessions', userData);
    yield put(loginUserSuccess(response.data.user));
    yield put(historyPush('/'));
    toast.success('Login successful');
  } catch (error) {
    toast.error(error.response.data.global);
    yield put(loginUserFailure(error.response.data));
  }
}

export function* facebookLoginSaga(action) {
  try {
    const response = yield axiosApi.post('/users/facebookLogin', action.payload);
    yield put(loginUserSuccess(response.data.user));
    yield put(historyPush('/'));
    toast.success('Login successful!');
  } catch (error) {
    toast.error(error.response.data.global);
    yield put(loginUserFailure(error.response.data));
  }
}

export function* logoutUserSaga() {
  try {
    yield axiosApi.delete('/users/sessions');
    yield put(logoutUser());
    yield put(historyPush('/'));
    toast.success('Logout out!');
  } catch (error) {
    toast.error('Logout failed');
  }
}

const usersSaga = [
  takeEvery(registerUser, registerUserSaga),
  takeEvery(loginUser, loginUserSaga),
  takeEvery(logoutRequest, logoutUserSaga),
  takeEvery(loginFacebook, facebookLoginSaga),
];

export default usersSaga;