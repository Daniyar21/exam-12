import {put, takeEvery} from 'redux-saga/effects';
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {fetchPhotosFailure, fetchPhotosRequest, fetchPhotosSuccess} from "../actions/photosActions";

export function* photosSagas() {
    try {
        const response = yield axiosApi.get('/photos');
        yield put(fetchPhotosSuccess(response.data));
    } catch (e) {
        yield put(fetchPhotosFailure(e));
        toast.error('Fetch photos failed');
    }
}

const photoSaga = [
    takeEvery(fetchPhotosRequest, photosSagas),
];

export default photoSaga;
