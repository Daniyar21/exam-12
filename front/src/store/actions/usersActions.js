import usersSlice from "../slices/usersSlice";

export const {
    registerUser,
    registerUserSuccess,
    registerUserFailure,
    loginUser,
    loginUserSuccess,
    loginUserFailure,
    clearErrorUser,
    logoutRequest,
    logoutUser,
    loginFacebook,
} = usersSlice.actions;