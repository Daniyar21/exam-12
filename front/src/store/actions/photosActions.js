
import photosSlice from "../slices/photosSlice";

export const {
    fetchPhotosRequest,
    fetchPhotosSuccess,
    fetchPhotosFailure,
} = photosSlice.actions;